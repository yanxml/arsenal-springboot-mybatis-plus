package com.yanxml.springboot.mybatis_plus.quickstart;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper.UserMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.baomidou.mybatisplus.core.enums.SqlKeyword.EQ;

@SpringBootTest
public class DynamicSelectTest {

    @Autowired private UserMapper userMapper;

    @Test
    public void queryAllUser1_Test() {
        String name = null;
        Integer age = null;
        String email = null;
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        if (StringUtils.isNotBlank(name)) {
            queryWrapper.eq("name", name);
        }
        if (age != null && age > 0) {
            queryWrapper.eq("age", age);
        }
        if (StringUtils.isNotBlank(email)) {
            queryWrapper.eq("email", email);
        }
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    /**
     * @Override public Children eq(boolean condition, R column, Object val) 使用API中的condition,
     * 进行动态SQL的筛选.
     */
    @Test
    public void queryAllUser2_Test() {
        String name = null;
        Integer age = null;
        String email = null;
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper
                .eq(StringUtils.isNotBlank(name), "name", name)
                .eq((age != null && age > 0), "age", age)
                .eq(StringUtils.isNotBlank(email), "email", email);
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }
}

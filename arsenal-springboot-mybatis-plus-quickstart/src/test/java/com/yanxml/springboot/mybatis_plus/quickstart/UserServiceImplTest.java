package com.yanxml.springboot.mybatis_plus.quickstart;

import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import com.yanxml.springboot.mybatis_plus.quickstart.service.IUserService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Junit for IUserService.
 */
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private IUserService userService;

    @Test
    public void selectById_Test() {
        User user = userService.getById(1l);
        System.out.println(user);
    }

    @Test
    public void selectList_Test() {
        List<User> userList = userService.list();
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void save_Test() {
        User user = new User();
        user.setName("yanxml");
        user.setEmail("admin@admin.com");
        user.setAge(20);
        boolean flag = userService.save(user);
        System.out.println("flag:"+flag);
        System.out.println("id:"+user.getId());
    }


    @Test
    public void saveBatch_Test() {
        User user1 = new User();
        user1.setName("yanxml1");
        user1.setEmail("admin1@admin.com");
        user1.setAge(20);
        User user2 = new User();
        user2.setName("yanxml2");
        user2.setEmail("admin2@admin.com");
        user2.setAge(20);
        // 批量插入的时候, 我们最好指定分批的条数.
//        boolean flag = userService.saveBatch(Lists.newArrayList(user1, user2);
        boolean flag = userService.saveBatch(Lists.newArrayList(user1, user2),2);
        System.out.println("flag:"+flag);
        System.out.println("id:"+user1.getId());
        System.out.println("id:"+user2.getId());
    }


}

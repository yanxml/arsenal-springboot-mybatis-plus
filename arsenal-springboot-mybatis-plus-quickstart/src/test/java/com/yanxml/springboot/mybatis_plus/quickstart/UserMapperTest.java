package com.yanxml.springboot.mybatis_plus.quickstart;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper.UserMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Junit for UserMapper.
 */
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void queryAllUser_Test() {
        List<User> users = userMapper.selectList(null);
        User printUser = CollectionUtils.isEmpty(users) ? null : users.get(0);
        System.out.println(printUser);
    }

    @Test
    public void insertUser_Test() {
        User user = new User();
        user.setName("sean");
        user.setAge(18);
        user.setEmail("admin@admin.com");
        int flag = userMapper.insert(user);
        System.out.println("flag:"+flag);
        System.out.println("flag:"+user.getId());
    }

    @Test
    public void updateUser_Test() {
        User user = new User();
        user.setId(1771507405178478593l);
        user.setName("sean");
        user.setAge(20);
        user.setEmail("admin@admin.com");
        userMapper.updateById(user);
    }

    @Test
    public void deleteUserById_Test() {
        userMapper.deleteById(1771508332698492930l);
    }

    @Test
    public void deleteUserByMap_Test() {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("name", "Jack");
        paramMap.put("age", 20);
        userMapper.deleteByMap(paramMap);
    }

    @Test
    public void deleteBatchById_test() {
        int i = userMapper.deleteBatchIds(Lists.newArrayList(1l, 2l, 3l, 4l));
        System.out.println(i);
    }

    @Test
    public void selectList_test() {
        List<User> userList = userMapper.selectList(null);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void selectById_test() {
        User user = userMapper.selectById(2l);
        System.out.println(user);
    }

    @Test
    public void selectBatchById_test() {
        List<User> userList = userMapper.selectBatchIds(Lists.newArrayList(1l,2l,3l,4l));
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void selectByMap_test() {
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("id", 1l);
        paramMap.put("name", "Jone");
        List<User> userList = userMapper.selectByMap(paramMap);
        for (User user : userList) {
            System.out.println(user);
        }
    }



}

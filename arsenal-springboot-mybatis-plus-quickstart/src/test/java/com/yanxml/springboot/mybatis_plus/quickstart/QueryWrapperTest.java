package com.yanxml.springboot.mybatis_plus.quickstart;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper.UserMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/** 使用QueryWrapper等进行复杂查询. */
@SpringBootTest
public class QueryWrapperTest {

    @Autowired private UserMapper userMapper;

    /** 姓名包含o, 年龄大于20, 且邮箱不为空的用户. */
    @Test
    public void queryAllUser_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.like("name", "o").gt("age", 20).isNotNull("email");
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    /** (姓名包含o, 年龄大于20), 或者 (邮箱不为空的用户). */
    @Test
    public void queryAllUser2_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.like("name", "o").gt("age", 20).or().isNotNull("email");
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void queryAllUser3_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper
                .and(
                        (item) -> {
                            item.like("name", "o").gt("age", 20);
                        })
                .or(
                        (item) -> {
                            item.isNotNull("email");
                        });
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void queryAllUser4_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper
                .and(
                        (item) -> {
                            item.like("name", "o").gt("age", 20);
                        })
                .or(
                        (item) -> {
                            item.isNotNull("email");
                        })
                .select("id", "name", "age");
        // 方式1. QueryWrapper进行筛选.
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void queryAllUser5_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper
                .and(
                        (item) -> {
                            item.like("name", "o").gt("age", 20);
                        })
                .or(
                        (item) -> {
                            item.isNotNull("email");
                        })
                .select("id", "name", "age");

        // 方式2. selectMaps
        List<Map<String, Object>> mapsList = userMapper.selectMaps(queryWrapper);
        mapsList.forEach((item) -> System.out.println(item));
    }

    /** 子查询 select * from user where id in (select id form user where id<6) */
    @Test
    public void queryAllUser6_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.inSql("id", "select id from user where id<6");
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    /** 姓名包含o, 年龄大于20, 且邮箱不为空的用户. 排序: 先根据年龄, 相同年龄根据id降序. */
    @Test
    public void queryAllUserByOrder_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("age").orderByDesc("id");
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user);
        }
    }

    /** 根据条件删除. */
    @Test
    public void deleteByWrapper_Test() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.eq("age", 28);
        int deleteCount = userMapper.delete(queryWrapper);
    }

    @Test
    public void updateByWrapper_Test() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper();
        updateWrapper.set("age", 33).eq("name", "yanxml"); // set是更新的操作 eq是后续的条件
        userMapper.update(null, updateWrapper);
    }
}

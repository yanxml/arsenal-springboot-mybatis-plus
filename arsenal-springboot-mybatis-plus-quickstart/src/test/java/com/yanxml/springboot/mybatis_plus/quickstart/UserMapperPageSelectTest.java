package com.yanxml.springboot.mybatis_plus.quickstart;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper.UserMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
public class UserMapperPageSelectTest {

    @Autowired private UserMapper userMapper;

    @Test
    public void queryAllUser1_Test() {
        Page<User> pageCondition = new Page<>(1, 5);
        Page<User> userPage = userMapper.selectPage(pageCondition, null);
        log.info(
                "total: {}, pageNo:{}, pageSize:{}",
                userPage.getTotal(),
                userPage.getCurrent(),
                userPage.getSize());
    }

    @Test
    public void queryAllUser2_Test() {
        List<OrderItem> orderItemList =
                Lists.newArrayList(OrderItem.asc("age"), OrderItem.desc("name"));
        Page<User> pageCondition = new Page<>(1, 5);
        pageCondition.setOrders(orderItemList);
        Page<User> userPage = userMapper.selectPage(pageCondition, null);
        log.info(
                "total: {}, pageNo:{}, pageSize:{}",
                userPage.getTotal(),
                userPage.getCurrent(),
                userPage.getSize());
    }
}

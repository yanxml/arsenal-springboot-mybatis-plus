package com.yanxml.springboot.mybatis_plus.quickstart.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@TableName("`user`")
public class User {
    // 表结构Id
//    @TableId
    // 当不一致时
//    @TableId(value = "u_id", type= IdType.AUTO)
    private Long id;
    // userName 数据库 user_name. mybatis会进行自动转换
//    @TableField(value = "name")
    private String name;
    private Integer age;
    private String email;

//    @TableLogic(value = "0", delval = "1")
//    private int isDeleted; // 逻辑删除.
}

package com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import org.springframework.stereotype.Component;

/**
 * MybatisPlus 中Mapper接口必须集成BaseMapper<User>
 *    同时指定相应的实体类.
 */
@Component
public interface UserMapper extends BaseMapper<User> {
}

package com.yanxml.springboot.mybatis_plus.quickstart.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;

/**
 * 自定义的Service
 * 需要继承IService接口.
 */
public interface IUserService extends IService<User> {
}

package com.yanxml.springboot.mybatis_plus.quickstart.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yanxml.springboot.mybatis_plus.quickstart.dao.mapper.UserMapper;
import com.yanxml.springboot.mybatis_plus.quickstart.pojo.User;
import com.yanxml.springboot.mybatis_plus.quickstart.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
}
